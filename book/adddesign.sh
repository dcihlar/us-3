#!/bin/bash
set -e

out="$1" # output file
shift
inp="$1" # input file
shift

# destroy output file on error
function on_error() {
	rm -f "$out"
	echo "$(basename "$0") failed!" >&2
}
trap on_error ERR

(
	function gen_img() {
		echo -e "![$1]($(basename "$2"))\n"
		hasdesign=y
	}

	cat "$inp"
	echo -e '\n### Design\n'

	hasdesign=n
	for media in "$@"
	do
		media="$(basename "$media")"

		pcbid='?'
		if [[ "$media" =~ -([0-9])[0-9]- ]]
		then
			pcbid=$(( ${BASH_REMATCH[1]} + 1 ))
		else
			continue
		fi

		case "$media" in
			*-[0-9]1-*.*)
				gen_img "PCB$pcbid Component Side" "$media"
				;;
			*-[0-9]2-*.*)
				gen_img "PCB$pcbid Back Side" "$media"
				;;
			*-03-*.*)
				gen_img "Module Cover" "$media"
				;;
			*-[1-9]3-*.*)
				gen_img "Module Cover Alternative $((pcbid - 1))" "$media"
				;;
			*-[0-9]4-*.*)
				gen_img "PCB$pcbid Components" "$media"
				;;
			*-[0-9]5-*.*)
				gen_img "PCB$pcbid Traces" "$media"
				;;
			*-[0-9]6-*.*)
				gen_img "Schematic $pcbid" "$media"
				;;
		esac
	done

	if [ $hasdesign = n ]
	then
		echo "_This module currently doesn't contain any design files._"
	fi
) > "$out"
