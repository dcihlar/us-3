## Amplifier

Operator hands-free phone.

It contains microphone and speaker amplifiers.
Phone interface is without a hybrid.
Instead it detects who currently speaks and switches accordingly.

Received voice has AGC and can be attenuated on the front panel potentiometer.
There is also squelch adjusting for the microphone.
