## Amplifier

Operator hands-free phone.

It contains microphone and speaker amplifiers.
Phone interface is without a hybrid.
Instead it detects who currently speaks and switches accordingly.

Received voice has AGC and can be attenuated on the front panel potentiometer.
There is also squelch adjusting for the microphone.

### Design

![PCB2 Component Side](Amp-11-front.lowres.jpg)

![PCB3 Component Side](Amp-21-front.lowres.jpg)

![PCB4 Component Side](Amp-31-front.lowres.jpg)

![PCB6 Component Side](Amp-51-front.lowres.jpg)

![Schematic 1](Amp-06-schematic.png)

![Schematic 2](Amp-16-schematic.png)

![Schematic 3](Amp-26-schematic.png)

![Schematic 4](Amp-36-schematic.png)

![Schematic 5](Amp-46-schematic.png)

![Schematic 6](Amp-56-schematic.png)

