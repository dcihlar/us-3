## 3C19

RRU interface module.

This module handles 4-wire link and connects it to one of two internal 2-wire links via 3V51 and 3V52.
Second link that is connected to an internal link will have tx/rx pairs crossed.

It also monitors if the external link is functional.
When a link is not connected it will send out a constant 2kHz tone from 3G16.
The same tone is detected on a receiving link.
An alarm signal will be generated when the tone disappears.

Default configuration detects the tone from around 50mV RMS.

### Design

![PCB1 Component Side](3C19-01-front.lowres.jpg)

![PCB2 Component Side](3C19-11-front.lowres.jpg)

![Schematic 1](3C19-06-schematic.png)

![Schematic 2](3C19-16-schematic.png)

