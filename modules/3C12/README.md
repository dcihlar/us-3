## 3C12

Interface module for four non-biased phone lines.

This is the simplest interface module.

### Design

![PCB1 Component Side](3C12-01-front.lowres.jpg)

![Schematic 1](3C12-06-schematic.png)

