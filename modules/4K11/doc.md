## 4K11

Measurement switchover and ring type selection.

RE1 is turned on when any one of the 3C19 modules is selected.
Outgoing ring will then use a specialized 3C19 signal instead
of the 75 VAC signal.

RE2 is used in conjunction with the rotary switches to select
a signal going to the level meter.
