## 3A5

Fault detector and forwarder.

One module supports 6 input groups of 8 signals, and 8 common output signals.
Each of the 6 groups can be selected and forwarded to output.
Also each group has "or" function for input signals.

Any active signal within a group will light the appropriate bulb on the top panel.
The group can then be selected so its outputs are forwarded.
