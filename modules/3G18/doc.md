## 3G18

Flasher and square wave generator.

Flasher is enabled when a bulb on a panel needs to blink.

The purpose of the square wave generator is unknown.
Its frequency is 770Hz with 200us off and 1.1ms on.
