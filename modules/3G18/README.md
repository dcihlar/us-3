## 3G18

Flasher and square wave generator.

Flasher is enabled when a bulb on a panel needs to blink.

The purpose of the square wave generator is unknown.
Its frequency is 770Hz with 200us off and 1.1ms on.

### Design

![PCB1 Component Side](3G18-01-front.lowres.jpg)

![Schematic 1](3G18-06-schematic.png)

