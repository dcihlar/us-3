## 3C15

Internal links handler.

Only two users can be on one link at the same time.
Inputs 1 and 25 count users of one of the two internal links.
I.e. if there are two users then Re1 (or Re2) will be activated and next link will be forced.
