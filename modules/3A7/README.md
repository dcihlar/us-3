## 3A7

Audible module with transformer for the ring signal generator.

Two audible sources are installed: a bell and a buzzer.
There are also two diodes which aren't connected on the backplane.

The module is a part of the signalling system.

### Design

![PCB1 Component Side](3A7-01-front.lowres.jpg)

![Schematic 1](3A7-06-schematic.png)

