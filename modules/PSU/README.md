## Power Supply Unit

-48V power supply.

Unfortunately only one was found and it was missing the crucial parts.

It probably works like the one on FDM.

### Design

![PCB1 Component Side](PSU-01-front.lowres.jpg)

![Schematic 1](PSU-06-schematic.png)

