## 3G17

Ring signal oscillator.

This oscillator drives transformer on 3A7 and produces high voltage AC signal
required to ring a bell on a phone line.

It is turned on only when needed.

### Design

![PCB1 Component Side](3G17-01-front.lowres.jpg)

![Schematic 1](3G17-06-schematic.png)

