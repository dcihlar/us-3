## 3G17

Ring signal oscillator.

This oscillator drives transformer on 3A7 and produces high voltage AC signal
required to ring a bell on a phone line.

It is turned on only when needed.
