## 4A6

Alarm sound selector.

Alarm type for TSC, RRU, UNF, and SIGN can be selected with this board.

Placing jumper from a middle pin to LA will select loud alarm (a bell),
and placing it to TA will select a silent alarm (a buzzer).

### Design

![PCB1 Component Side](4A6-01-front.lowres.jpg)

