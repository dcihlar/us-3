## 3C18

Interface module with answering.

This module is like 3C13, but with additional coil for acting as an off-hook phone.

### Variants

3C18 has a few variants that are selected with jumpers.

On the schematic variants are printed in dark blue.

The varinats are:

 * A = FDM,
 * B = unknown,
 * C = ATC,
 * D = ATA.

"-" means the jumper is closed for a variant,
and "x" means the jumper is opened.
Arrows point to a number where a point should be connected.
