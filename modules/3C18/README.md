## 3C18

Interface module with answering.

This module is like 3C13, but with additional coil for acting as an off-hook phone.

### Variants

3C18 has a few variants that are selected with jumpers.

On the schematic variants are printed in dark blue.

The varinats are:

 * A = FDM,
 * B = unknown,
 * C = ATC,
 * D = ATA.

"-" means the jumper is closed for a variant,
and "x" means the jumper is opened.
Arrows point to a number where a point should be connected.

### Design

![PCB1 Component Side](3C18-01-front.lowres.jpg)

![PCB1 Traces](3C18-05-traces.lowres.jpg)

![Schematic 1](3C18-06-schematic.png)

![Schematic 2](3C18-16-schematic.png)

![Schematic 3](3C18-26-schematic.png)

![Schematic 4](3C18-36-schematic.png)

![Schematic 5](3C18-46-schematic.png)

![Schematic 6](3C18-56-schematic.png)

