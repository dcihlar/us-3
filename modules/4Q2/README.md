## 4Q2

Right phone on the bottom panel.

It also handles separating and joining left and right segments.

### Design

![PCB1 Component Side](4Q2-01-front.lowres.jpg)

![Schematic 1](4Q2-06-schematic.png)

