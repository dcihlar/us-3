## 3C22

2W to 4W upstream converter.

It handles upstream link as configured on the top panel.

### Design

![PCB1 Component Side](3C22-01-front.lowres.jpg)

![Schematic 1](3C22-06-schematic.png)

