## 3V51

Hybrid and relays.

Hybrid is used by the top panel to connect to the internal 4W link.

Relays are used to help with 2W/4W conversion for 3C19.

### Design

![PCB1 Component Side](3V51-01-front.lowres.jpg)

![Schematic 1](3V51-06-schematic.png)

