## 3C13

Interface module.

Line can be connected to one of two internal links.

It can be configured for biased and non-biased link.

### Configuration

Link configuration is set with jumpers.
"-" represent inserted jumper, and "/" represents open connection.

Non-biased links (as on schematic):

 * 1-2, 3-4
 * 5/6-7,
 * 8-9,
 * 10-11,
 * 12-13/14,
 * 15/16,
 * 17-18,
 * 19-20,
 * 21-22,
 * 23/24,
 * 25/26-27,
 * 28/29-30/31.

Biased links:

 * 1/2, 3/4
 * 5-6/7,
 * 8/9,
 * 10/11,
 * 12/13-14,
 * 15-16,
 * 17/18,
 * 19/20,
 * 21/22,
 * 23-24,
 * 25-26/27,
 * 28-29/30-31.
