## 3V52

Hybrids.

Hybrids for converting 4W coming from 3C19 to an internal link.

### Design

![PCB1 Component Side](3V52-01-front.lowres.jpg)

![Schematic 1](3V52-06-schematic.png)

