## 3C16-a

Upstream interface.

When link is selected it will act as a phone goes off hook.

Upstream can be connected to only one internal interface.

### Design

![PCB1 Component Side](3C16a-01-front.lowres.jpg)

![Schematic 1](3C16a-06-schematic.png)

