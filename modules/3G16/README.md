## 3G16

Tone generator.

Two tones are generated: 2kHz and 437Hz.

2kHz tone is generated constantly, but 437Hz needs to be enabled with pin 23.

### Design

![PCB1 Component Side](3G16-01-front.lowres.jpg)

![Schematic 1](3G16-06-schematic.png)

