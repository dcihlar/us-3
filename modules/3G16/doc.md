## 3G16

Tone generator.

Two tones are generated: 2kHz and 437Hz.

2kHz tone is generated constantly, but 437Hz needs to be enabled with pin 23.
