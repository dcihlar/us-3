## Fuses

Upper row of fuses:

| Column | Name                | Fault group |
|--------|---------------------|-------------|
| 1      | TSC#1               | d           |
| 2      | TSC#2               | d           |
| 3      | TSC#3               | d           |
| 4      | TSC#4               | d           |
| 5      | TSC#5               | d           |
| 6      | TSC#6               | d           |
| 7      | TSC#7               | d           |
| 8      | TSC#8               | d           |
| 9      | TSC#9               | d           |
| 10     | TSC#10              | d           |
| 11     | TSC#11              | d           |
| 12     | TSC#12              | d           |
| 13     | Lights X2Y5=PL2.E16 | d           |
| 14     |                     | d           |
| 15     | sign60-100=PL2.E17  | d           |
| 16     | 3A5#1               | d           |
| 17     |                     | d           |
| 18     | x                   | d           |
| 19     | x                   | d           |
| 20     | x                   | d           |
| 21     | x                   | c           |
| 22     |                     | c           |
| 23     | x                   | c           |
| 24     | Roof PCB            | c           |
| 25     | Lights X3Y2         | c           |
| 26     | Lights X2Y1         | c           |
| 27     |                     | c           |
| 28     | Lights X3Y1         | c           |
| 29     | 4Q1.40              | d           |
| 30     | 4Q1.41              | c           |

Lower row of fuses:

| Column | Name                | Fault group |
|--------|---------------------|-------------|
| 1      | 3C12#1              | d           |
| 2      | 3C12#2              | d           |
| 3      | 3C12#3              | d           |
| 4      | 3C12#4              | d           |
| 5      | 3C12#5              | d           |
| 6      | 3C13#1-5            | d           |
| 7      | 3C13#6-10           | d           |
| 8      | Lights X2aY2        | d           |
| 9      | Lights X2bY2        | d           |
| 10     | PL9.D8+lightsX3aY5  | d           |
| 11     | PL9.C8+lights X3bY5 | d           |
| 12     | PL9.B8+lightsX3aY3  | d           |
| 13     | PL9.A8+lightsX3bY3  | d           |
| 14     | 3C16 box            | d           |
| 15     | x                   | d           |
| 16     |                     | d           |
| 17     | x                   | d           |
| 18     | x                   | d           |
| 19     | x                   | d           |
| 20     | Svetlo              | d           |
| 21     | 3C15#1              | c           |
| 22     | 3C15#2              | c           |
| 23     | 3G16                | c           |
| 24     | 3G18                | c           |
| 25     | Bell                | c           |
| 26     | 3V51                | c           |
| 27     | 4Q1.27              | c           |
| 28     | 4Q2.34              | c           |
| 29     | Sel 60 (PL6E20)     | c           |
| 30     | x                   | c           |
