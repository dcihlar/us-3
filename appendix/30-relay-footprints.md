## Relay footprints

Relay footprints (pinouts) used for reverse engineering:

### Vertical

![PR15 and PR16 pinout](PR156-pinout.png)

![PR17 pinout](PR17-pinout.png)

### Horizontal

![PR15 angled socket pinout](PR15-side-footprint.png)

![PR16 angled socket pinout](PR16-side-footprint.png)
