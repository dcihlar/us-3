## Selection schematics

The following schematics were used to figure out the high level connections.

### 3C12 selection

![W1](W1.png)

### 3C13 and 3C19 selection

![W2](W2.png)

### 3C18 selection

![W3](W3.png)
