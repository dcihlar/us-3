## Signalling

### Radio buttons

![HLS1](HLS1.png)

### Alarming

Fuse fault and external source alarming.

![HLS4](HLS4.png)

### Middle block

Signal wiring between 3C19, 3C13 and 3C15-1.

![HLS3](HLS3.png)

### Right block

Signal wiring between 3C18, 3C15 and 3C16-a.

![HLS2](HLS2.png)
