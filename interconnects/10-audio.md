## Audio

### Left block

This diagram includes left and middle blocks.

![HLA1](HLA1.png)


### Right block

This diagram includes right block and handsets.

![HLA2](HLA2.png)


### Speaker source

4Q1 selects if a pulsing tone or the amplifier is connected to the speaker.

![HLA3](HLA3.png)
