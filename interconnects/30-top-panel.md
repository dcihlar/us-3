## Vertical panel

Signal and audio wiring of the vertical (top) panel:

![top-panel](top-panel.png)
